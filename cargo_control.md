### [创建工作空间](https://rustwiki.org/zh-CN/book/ch14-03-cargo-workspaces.html#创建工作空间)

创建文件夹

```

$ mkdir add
$ cd add
```

文件名: Cargo.toml

```

[workspace]

members = [
    "adder",
]
```

接下来，在 *add* 目录运行 `cargo new` 新建 `adder` 二进制 crate：

```

$ cargo new adder
     Created binary (application) `adder` project
```

到此为止，可以运行 `cargo build` 来构建工作空间。*add* 目录中的文件应该看起来像这样：

```

├── Cargo.lock
├── Cargo.toml
├── adder
│   ├── Cargo.toml
│   └── src
│       └── main.rs
└── target
```

### [在工作空间中创建第二个 crate](https://rustwiki.org/zh-CN/book/ch14-03-cargo-workspaces.html#在工作空间中创建第二个-crate)

接下来，让我们在工作空间中指定另一个成员 crate。这个 crate 位于 *add-one* 目录中，所以修改顶级 *Cargo.toml* 为也包含 *add-one* 路径：

文件名: Cargo.toml

```

[workspace]

members = [
    "adder",
    "add-one",
]
```

```text
$ cargo new add-one --lib
     Created library `add-one` project
```

现在 *add* 目录应该有如下目录和文件：

```

├── Cargo.lock
├── Cargo.toml
├── add-one
│   ├── Cargo.toml
│   └── src
│       └── lib.rs
├── adder
│   ├── Cargo.toml
│   └── src
│       └── main.rs
└── target
```

文件名: adder/src/main.rs

```

use add_one;

fn main() {
    let num = 10;
    println!("Hello, world! {} plus one is {}!", num, add_one::add_one(num));
}
```



现在工作空间中有了一个库 crate，让 `adder` 依赖库 crate `add-one`。首先需要在 *adder/Cargo.toml* 文件中增加 `add-one` 作为路径依赖：

文件名: adder/Cargo.toml

```

[dependencies]

add-one = { path = "../add-one" }
```

文件名: adder/src/main.rs

```

use add_one;

fn main() {
    let num = 10;
    println!("Hello, world! {} plus one is {}!", num, add_one::add_one(num));
}
```

在 *add* 目录中运行 `cargo build` 来构建工作空间！

```

$ cargo build
```

们希望使用的包：

```

$ cargo run -p adder
```

#### [在工作空间中依赖外部 crate](https://rustwiki.org/zh-CN/book/ch14-03-cargo-workspaces.html#在工作空间中依赖外部-crate)

还需注意的是工作空间只在根目录有一个 *Cargo.lock*，而不是在每一个 crate 目录都有 *Cargo.lock*。这确保了所有的 crate 都使用完全相同版本的依赖。如果在 *Cargo.toml* 和 *add-one/Cargo.toml* 中都增加 `rand` crate，则 Cargo 会将其都解析为同一版本并记录到唯一的 *Cargo.lock* 中。使得工作空间中的所有 crate 都使用相同的依赖意味着其中的 crate 都是相互兼容的。让我们在 *add-one/Cargo.toml* 中的 `[dependencies]` 部分增加 `rand` crate 以便能够在 `add-one` crate 中使用 `rand` crate：

文件名: add-one/Cargo.toml

```

[dependencies]
rand = "0.5.5"
```

作空间就会引入并编译 `rand` crate：

```

$ cargo build
```

文件名: add-one/src/lib.rs

```

pub fn add_one(x: i32) -> i32 {
    x + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        assert_eq!(3, add_one(2));
    }
}
```

在顶级 *add* 目录运行 `cargo test`：

```

$ cargo test
```

也可以选择运行工作空间中特定 crate 的测试，通过在根目录使用 `-p` 参数并指定希望测试的 crate 名称：

```

$ cargo test -p add-one
```

