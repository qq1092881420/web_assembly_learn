Git 全局设置

 git config --global user.name "xx" 
 git config --global user.email "xx@noreply.gitcode.com" 

创建一个新仓库

 git clone https://gitcode.com/qq1092881420/web_assembly_learn.git
 cd web_assembly_learn 
 echo "# web_assembly_learn" >> README.md 
 git add README.md 
 git commit -m "add README" 
 git branch -m main 
 git push -u origin main 

推送现有的文件

 cd existing_folder 
 git init 
 git remote add origin https://gitcode.com/qq1092881420/web_assembly_learn.git 
 git add . 
 git commit -m "Initial commit" 
 git branch -m main 
 git push -u origin main 

推送现有的 Git 仓库

 cd existing_repo 
 git remote rename origin old-origin 
 git remote add origin https://gitcode.com/qq1092881420/web_assembly_learn.git 
 git push -u origin --all 
 git push -u origin --tags 



暂存区查看

```
git ls-files --cached
```