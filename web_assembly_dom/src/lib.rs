use wasm_bindgen::prelude::*;


#[wasm_bindgen(start)]
fn run() -> Result<(), JsValue> {
    // Use `web_sys`'s global `window` function to get a handle on the global
    // window object.
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");
    let body = document.body().expect("document should have a body");
    let head = document.head().expect("document should have a head");
    let script_a = document.create_element("script")?;
    let script_b = document.create_element("script")?;
    let style = document.create_element("style")?;
    let link_css_a = document.create_element("link")?;
    let link_css_b = document.create_element("link")?;


    let div_a = document.create_element("div")?;
    let div_carousel = document.create_element("div")?;
    // Manufacture the element we're gonna append
    let p = document.create_element("p")?;
    let button = document.create_element("button")?;

    // <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    //     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js" integrity="sha384-ep+dxp/oz2RKF89ALMPGc7Z89QFa32C8Uv1A3TcEK8sMzXVysblLA3+eJWTzPJzT" crossorigin="anonymous"></script>

    head.append_child(&link_css_a)?;
    head.append_child(&link_css_b)?;
    head.append_child(&style)?;
    head.append_child(&script_a)?;
    head.append_child(&script_b)?;

    link_css_a.clone().set_attribute("rel","stylesheet")?;
    link_css_a.clone().set_attribute("href","https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css")?;
    link_css_b.clone().set_attribute("rel","stylesheet")?;
    link_css_b.clone().set_attribute("href","https://getbootstrap.com/docs/5.3/assets/css/docs.css")?;


    // link_script_a.clone().set_attribute("src","https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js")?;


    script_a.set_attribute("src","https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js")?;



    // Append `p` to `body`
    body.append_child(&div_a)?;
    // A slideshow component for cycling through elements—images or slides of text—like a carousel.
    body.append_child(&div_carousel)?;


    // Get the text content of the body
    div_a.set_id("my_div_id");
    p.set_class_name("my_p_class");
    button.set_class_name("my_button_class");

    let my_div_id = document.get_element_by_id(&"my_div_id");
    let my_p_class = document.get_elements_by_class_name(&"my_p_class");
    let my_button_class = document.get_elements_by_class_name(&"my_button_class");




    my_div_id.clone().unwrap().set_inner_html("is div<span>is span</span>");

    my_div_id.clone().unwrap().append_child(&p)?;
    my_div_id.clone().unwrap().append_child(&button)?;





    my_p_class.get_with_index(0).unwrap().set_text_content(Some("Hello from Rust!"));
    my_button_class.get_with_index(0).unwrap().set_text_content(Some("click me"));

    my_div_id.clone().unwrap().set_attribute("style","margin:20px")?;
    my_p_class.get_with_index(0).unwrap().set_attribute("style","color:rgba(124,80,154,0.6)")?;
    my_button_class.get_with_index(0).unwrap().set_attribute("style","border:1px solid #ac245d;background-color:#11dd0c;color:#ffffff;padding:10px;font-size:16px;")?;

    style.set_attribute("type" , "text/css")?;
    style.set_text_content(Some("span {color: #11dc4c;font-weight: bold} img{height: 800px;width: 800px}"));




    let str = format!("
<div class='container' style='margin-top:20px'>
    <div id='carouselExampleCaptions' class='carousel slide' data-ride='carousel'>
      <ol class='carousel-indicators'>
        <li data-target='#carouselExampleCaptions' data-slide-to='0' class='active'></li>
        <li data-target='#carouselExampleCaptions' data-slide-to='1'></li>
        <li data-target='#carouselExampleCaptions' data-slide-to='2'></li>
      </ol>
      <div class='carousel-inner'>
        <div class='carousel-item active'>
          <img src='https://pic.rmb.bdstatic.com/bjh/be0bb14163f5b6fc4a4331393469e9939764.jpeg' class='d-block w-100' alt='...'>
          <div class='carousel-caption d-none d-md-block'>
          </div>
        </div>
        <div class='carousel-item'>
          <img src='https://q8.itc.cn/images01/20240223/b19552de15f1430694449ddbd8977c3b.jpeg' class='d-block w-100' alt='...'>
          <div class='carousel-caption d-none d-md-block'>
          </div>
        </div>
        <div class='carousel-item'>
          <img src='https://i0.hdslb.com/bfs/archive/ee6b6d80b51370a9e010717d91459016b466d9b6.jpg' class='d-block w-100' alt='...'>
          <div class='carousel-caption d-none d-md-block'>
          </div>
        </div>
      </div>
      <button class='carousel-control-prev' type='button' data-target='#carouselExampleCaptions' data-slide='prev'>
        <span class='carousel-control-prev-icon' aria-hidden='true'></span>
        <span class='sr-only'>Previous</span>
      </button>
      <button class='carousel-control-next' type='button' data-target='#carouselExampleCaptions' data-slide='next'>
        <span class='carousel-control-next-icon' aria-hidden='true'></span>
        <span class='sr-only'>Next</span>
      </button>
    </div>
</div>
").as_str();
    div_carousel.clone().set_outer_html(str);


    Ok(())
}



